require 'test_helper'

class PantryItemTest < ActiveSupport::TestCase

  def setup
    @pantry_item = PantryItem.new(name: "Oranges")
  end

  test "should be valid" do
    assert @pantry_item.valid?
  end

  test "name should be present" do
    @pantry_item.name = ""
    assert_not @pantry_item.valid?
  end

  test "name should be unique" do
    duplicate_pantry_item = @pantry_item.dup
    duplicate_pantry_item.name = @pantry_item.name.upcase
    @pantry_item.save
    assert_not duplicate_pantry_item.valid?
  end

end
