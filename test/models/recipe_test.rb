require 'test_helper'

class RecipeTest < ActiveSupport::TestCase

	#gets run every time before each test
	def setup
		#declare a recipe instance variable
		@recipe = Recipe.new(name: "A newer recipe", ingredients: "A list of ingredients",
			directions: "Step by step directions", recipe_status: "Finalized")
	end

	#test to confirm a new user created is validated (and to confirm user created for testing purposes is valid)
	test "should be valid" do
		assert @recipe.valid?
	end

	#test to confirm a name is present
	test "name should be present" do
		#reset the default testing instance variable
		@recipe.name = ""
		assert_not @recipe.valid?
	end

	test "ingredients should be present" do
		@recipe.ingredients = ""
		assert_not @recipe.valid?
	end

	test "directions should be present" do
		@recipe.directions = ""
		assert_not @recipe.valid?
	end

	#make sure a recipe with that same name has not been entered
	#want to make sure uniqueness is case insensitive so we compare normal type to uppercase to confirm case insensitive
	test "name should be unique" do
		duplicate_recipe = @recipe.dup
		duplicate_recipe.name = @recipe.name.upcase
		@recipe.save
		assert_not duplicate_recipe.valid?
	end

	test "recipe_status should be present" do
		@recipe.recipe_status = ""
		assert_not @recipe.valid?
	end

	test "recipe_status should be one of listed" do
		@recipe.recipe_status = "Not listed"
		assert_not @recipe.valid?
	end

end
