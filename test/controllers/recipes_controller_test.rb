require 'test_helper'

class RecipesControllerTest < ActionDispatch::IntegrationTest

  setup do
    @recipe = recipes(:one)
  end

  #test recipe#index
  test "should get index" do
    get recipes_url
    assert_response :success
  end

  #test recipe#new
  test "should get new" do
    get new_recipe_url
    assert_response :success
  end

  #test recipe#create
  test "should create recipe" do
    assert_difference('Recipe.count') do
      post recipes_url, params: { recipe: { name: "New Name Here",
        ingredients: @recipe.ingredients, directions: @recipe.directions,
        recipe_status: @recipe.recipe_status } }
    end
    assert_redirected_to recipe_url(Recipe.last)
  end

  #test recipe#show
  test "should get show" do
    get recipe_url(@recipe)
    assert_response :success
  end

  #test recipe#edit
  test "should get edit" do
    get edit_recipe_url(@recipe)
    assert_response :success
  end

  #test recipe#update
  test "should update recipe" do
    patch recipe_url(@recipe), params: { recipe: { name: "Edited Name Here",
      ingredients: @recipe.ingredients, directions: @recipe.directions } }
    assert_redirected_to recipe_url(@recipe)
  end

  #test recipe#destroy
  test "should destroy recipe" do
    assert_difference('Recipe.count', -1) do
      delete recipe_url(@recipe)
    end
    assert_redirected_to recipes_url
  end

end
