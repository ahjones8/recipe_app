require 'test_helper'

class TagsControllerTest < ActionDispatch::IntegrationTest

  test "should get index" do
    get tags_url
    assert_response :success
  end

  test "should get new" do
    get new_tag_url
    assert_response :success
  end

  test "should create recipe" do
    assert_difference('Tag.count') do
      post tags_url, params: {tag: {name: "Tag name"}}
    end
    assert_redirected_to tags_url
  end
  
end
