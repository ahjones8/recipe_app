require 'test_helper'

class PantryItemsControllerTest < ActionDispatch::IntegrationTest

  setup do
    @pantry_item = pantry_items(:one)
  end

  test "should get new" do
    get new_recipe_url
    assert_response :success
  end

  test "should get index" do
    get pantry_items_url
    assert_response :success
  end

  test "should get edit" do
    get edit_recipe_url(@pantry_item)
    assert_response :success
  end

  test "should create new pantry item" do
    assert_difference('PantryItem.count') do
      post pantry_items_url, params: {pantry_item: {name:"Test7"}}
    end
    assert_redirected_to pantry_items_url
  end

  test "should update pantry item" do
    patch pantry_item_url(@pantry_item), params: {pantry_item: {name: "Test8"}}
    assert_redirected_to pantry_item_url(@pantry_item)
  end

end
