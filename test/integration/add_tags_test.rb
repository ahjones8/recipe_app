require 'test_helper'

class AddTagsTest < ActionDispatch::IntegrationTest
  test "invalid tag addition" do
    get new_tag_path
    assert_no_difference 'Tag.count' do
      post tags_path, params: { tag: {name: ""}}
    end
    assert_template 'tags/new'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
  end

  test "valid tag addition" do
    get new_tag_path
    assert_difference 'Tag.count' do
      post tags_path, params: { tag: {name: "Tag"}}
    end
    follow_redirect!
    assert_template 'tags/index'
  end
end
