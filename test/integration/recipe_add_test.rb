require 'test_helper'

class RecipeAddTest < ActionDispatch::IntegrationTest
  test "invalid recipe addition" do
    get new_recipe_path
    assert_no_difference 'Recipe.count' do
      post recipes_path, params: { recipe: { name: "",
                                            ingredients: "",
                                            directions: ""}}
    end
    assert_template 'recipes/new'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
  end

  test "valid recipe addition" do
    get new_recipe_path
    assert_difference 'Recipe.count' do
      post recipes_path, params: { recipe: { name: "A new recipe",
                                directions: "List of directions",
                                ingredients: "List of ingredients",
                                recipe_status: "Finalized"}}
    end
    follow_redirect!
    assert_template 'recipes/show'
  end
end
