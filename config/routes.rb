Rails.application.routes.draw do
  resources :categories
  root 'recipes#index'
  resources :recipes do
    member do
      get :tags
      post :tag_add
      post :tag_remove
    end
  end

  resources :tags do
    member do
      get :recipe_list
    end
  end

  resources :pantry_items do
    member do
      get :tags
      post :tag_add
      post :tag_remove
    end
  end

  resources :meal_plans do
    member do
      post :update_finished
      get :grocery_list
    end
  end

  resources :meal_plan_recipe_items

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
