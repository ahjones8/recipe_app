module RecipesHelper
  def source_name (source_link)
    if source_link != nil
      if source_link.include? "www."
        source_link[source_link.index(".")+1..source_link.index(".",source_link.index(".")+1)-1]
      elsif source_link.include? "http"
        source_link[source_link.index("//")+"//".length..source_link.index(".")-1]
      else
        source_link
      end
    end
  end

  def sort_link(column, title = nil)
    title ||= column.titleize
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    icon = sort_direction == "asc" ? "glyphicon glyphicon-chevron-up" :
      "glyphicon glyphicon-chevron-down"
    icon = column == sort_column ? icon : ""
    link_to "#{title} <span class='#{icon}'></span>".html_safe, {column: column, direction: direction}
  end
end
