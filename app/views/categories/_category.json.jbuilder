json.extract! category, :id, :name, :order_num, :created_at, :updated_at
json.url category_url(category, format: :json)
