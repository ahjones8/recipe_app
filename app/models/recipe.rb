class Recipe < ApplicationRecord
	#make sure each attribute is entered
	validates :name, presence: true, uniqueness: {case_sensitive: false}
	validates :ingredients, presence: true
	validates :directions, presence: true
	has_many :meal_plan_recipe_items

	RECIPE_STATUS = ['Not tried', 'In the works', 'Finalized']
	validates :recipe_status, presence: true, inclusion: RECIPE_STATUS

	#a recipe can have many tags and a tags belong to many recipes
	has_and_belongs_to_many :tags

	if Rails.env.development? or Rails.env.testing?
		scope :recipe_contains, -> (name) {where("name like ?", "%#{name}%")}
		scope :ingredients_contains, -> (ingredients) {where("ingredients like ?",
			"%#{ingredients}%")}
	else
		scope :recipe_contains, -> (name) {where("name ilike ?", "%#{name}%")}
		scope :ingredients_contains, -> (ingredients) {where("ingredients ilike ?",
			"%#{ingredients}%")}
	end
end
