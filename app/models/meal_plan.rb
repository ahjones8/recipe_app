class MealPlan < ApplicationRecord
  has_many :meal_plan_recipe_items, dependent: :destroy
end
