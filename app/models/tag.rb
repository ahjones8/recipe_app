class Tag < ApplicationRecord
  has_and_belongs_to_many :recipes
  has_and_belongs_to_many :pantry_items

  validates :name, presence: true, uniqueness: {case_sensitive: false}

end
