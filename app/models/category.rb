class Category < ApplicationRecord
  #both of these present at database level
  #validates_uniqueness_of :name
  #validates_uniqueness_of :order_num

  validates_presence_of :order_num
  validates_presence_of :name
end
