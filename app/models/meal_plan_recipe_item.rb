class MealPlanRecipeItem < ApplicationRecord
  belongs_to :recipe
  belongs_to :meal_plan
  belongs_to :category
end
