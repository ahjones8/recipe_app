class PantryItemsController < ApplicationController
  before_action :set_pantry_item, only: [:show, :edit, :update, :destroy]

  def new
    @pantry_item = PantryItem.new
  end

  def edit
  end

  def show
    redirect_to action: "tags"
  end

  def create
    @pantry_item = PantryItem.new(pantry_item_params)
    if @pantry_item.save
      redirect_to action: "index"
    else
      render 'new'
    end
  end

  def update
    if @pantry_item.update(pantry_item_params)
      redirect_to @pantry_item
    else
      render 'edit'
    end
  end

  def index
    @pantry_items = PantryItem.order("name")
  end

  def tags
    @pantry_item = PantryItem.find(params[:id])
    @tags = @pantry_item.tags.order("name")
  end

  def tag_add
    @pantry_item = PantryItem.find(params[:id])
    tag_ids = params[:tags]

    if tag_ids.any?
      tag_ids.each do |tag_id|
        tag = Tag.find(tag_id)
        logger.info "Adding tag #{tag.id}"
        @pantry_item.tags << tag
      end
    end
    redirect_to action: "tags", id: @pantry_item
  end

  def tag_remove
    @pantry_item = PantryItem.find(params[:id])
    tag_ids = params[:tags]

    if tag_ids.any?
      tag_ids.each do |tag_id|
        tag = Tag.find(tag_id)
        logger.info "Removing tag #{tag.id}"
        @pantry_item.tags.delete(tag)
      end
    end
    redirect_to action: "tags", id: @pantry_item
  end

  private
    def pantry_item_params
      params.require(:pantry_item).permit(:name)
    end

    def set_pantry_item
      @pantry_item = PantryItem.find(params[:id])
    end
end
