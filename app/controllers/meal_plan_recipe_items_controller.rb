class MealPlanRecipeItemsController < ApplicationController
before_action :set_meal_plan, only: [:create]

  def create
    recipe = Recipe.find(params[:recipe_id])
    @line_item = @meal_plan.meal_plan_recipe_items.build(recipe: recipe, category: Category.where(:name => "Unknown").first)
    if @line_item.save
      redirect_to @meal_plan
    end
  end

  private
    def meal_plan_recipe_item_params
      params.require(:meal_plan_recipe_item).permit(:recipe_id, :meal_plan_id)
    end

    def set_meal_plan
        @meal_plan = MealPlan.where("finished = ?",false).last
	if @meal_plan.nil?
	    name = "Week" + Time.now.strftime("%U") + "_" + (Time.current.year).to_s
	    @meal_plan = MealPlan.create(name: name)
        end
    end

end
