class MealPlansController < ApplicationController
before_action :set_meal_plan, only: [:show, :edit, :update, :destroy, :update_finished, :grocery_list]

  def index
    @meal_plans = MealPlan.order("id DESC")
  end

  def show
    @meal_plan_recipe_items = MealPlanRecipeItem.joins(:category).where(:meal_plan_id => @meal_plan.id).order("categories.order_num")
  end

  def update_finished
    @meal_plan.finished = true
    @meal_plan.save
    redirect_to @meal_plan
  end

  def grocery_list
    @recipes = @meal_plan.meal_plan_recipe_items
  end

  private
    def meal_plan_params
      params[:meal_plan]
    end

    def set_meal_plan
      @meal_plan = MealPlan.find(params[:id])
    end
end
