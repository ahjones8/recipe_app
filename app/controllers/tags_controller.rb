class TagsController < ApplicationController

  def new
    @tag = Tag.new
  end

  def create
    @tag = Tag.new(tag_params)
    if @tag.save
      redirect_to tags_path
    else
      render 'new'
    end
  end

  def index
    @tags = Tag.order("name")
  end

  def recipe_list
    @tag = Tag.find(params[:id])
  end

  private
    def tag_params
      params.require(:tag).permit(:name)
    end
end
