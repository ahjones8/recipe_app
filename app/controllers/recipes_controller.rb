class RecipesController < ApplicationController
#before any of these methods are run, this before_action is run to declare
#an instance of the controller to then pass along
before_action :set_recipe, only: [:show, :edit, :update, :destroy]
helper_method :sort_column, :sort_direction

  #GET /recipes/new
  def new
  	@recipe = Recipe.new
  end

  #GET /recipes/1
  def show
  end

  #POST /recipes
  def create
  	@recipe = Recipe.new(recipe_params)
    #debugger
    if @recipe.save
      #flash[:success] = "Recipe Added!"
      redirect_to @recipe
    else
      render 'new'
    end
  end

  #GET /recipes/1/edit
  def edit
  end

  #PATCH /recipes/1
  def update
    if @recipe.update(recipe_params)
      flash[:success] = "Recipe Updated!"
      redirect_to @recipe
    else
      render 'edit'
    end
  end

  #GET /recipes
  def index
    @recipes = Recipe.order("UPPER(#{sort_column}) #{sort_direction}")
    @recipes = @recipes.paginate :page => params[:page], :per_page => 30
    @recipes = @recipes.recipe_contains(params[:recipe_contains]) if params[:recipe_contains].present?
    @recipes = @recipes.ingredients_contains(params[:ingredients_contains]) if params[:ingredients_contains].present?
  end

  #DELETE /recipes/1
  def destroy
    @recipe.destroy
    flash[:danger] = "Recipe Deleted!"
    redirect_to recipes_path
  end

  #GET /recipes/1/tags
  def tags
    @recipe = Recipe.find(params[:id])
    @tags = @recipe.tags.order("name")
  end

  # POST /recipes/1/tag_add?tag_id=2
  def tag_add
    @recipe = Recipe.find(params[:id])
    @tag = Tag.find(params[:tag])
    @recipe.tags << @tag
    redirect_to action: "tags", id: @recipe
  end

  # POST /recipes/1/tag_remove?tags[]=
  def tag_remove
    @recipe = Recipe.find(params[:id])
    tag_ids = params[:tags]

    if tag_ids.any?
      tag_ids.each do |tag_id|
        tag = Tag.find(tag_id)
        logger.info "Removing student from tag #{tag.id}"
        @recipe.tags.delete(tag)
      end
    end
    redirect_to action: "tags", id: @recipe
  end

  private
  	def recipe_params
  		params.require(:recipe).permit(:name, :ingredients, :directions,
        :notes, :source, :recipe_status)
  	end

    def set_recipe
      @recipe = Recipe.find(params[:id])
    end

    def sortable_columns
      ["Name", "Source"]
    end

    def sort_column
      sortable_columns.include?(params[:column]) ? params[:column] : "name"
    end

    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end
end
