Tag.create(name: "Leftovers Lunch")
Tag.create(name: "Dinner")
Tag.create(name: "Breakfast")
Tag.create(name: "Dessert")
Tag.create(name: "Weeknight Meal")
Tag.create(name: "Weekend Meal")
Tag.create(name: "Freezer Friendly")

Category.create(name: "Unknown", order_num: 0)

Recipe.create(name: "Cranberry-orange breakfast buns",
      ingredients: "Dough:
        4 large egg yolks
        1 large whole egg
        1/4 cup (50 grams) granulated sugar
        6 tablespoons (85 grams) butter, melted, plus additional to grease pan
        3/4 cup (175 ml) buttermilk
        Zest of 1 orange, finely grated (to be used in dough and filling, below)
        3 3/4 cups (470 grams) all-purpose flour, plus more for dusting counter
        1 packet (7 grams or 2 1/4 teaspoons) instant dry yeast
        1 1/4 teaspoons coarse or kosher salt, or more to taste
        1 teaspoon oil for bowl

        Filling:
        1 1/2 tablespoons (20 grams) butter
        1 cup (190 grams) packed light brown sugar
        1 cup (115 grams) fresh cranberries
        Orange zest leftover from above

        Icing:
        3 1/2 tablespoons (55 ml) orange juice
        2 cups (240 grams) powdered sugar",
      directions: "Make the dough: In the bottom of the bowl of a stand mixer, whisk the yolks, whole egg, sugar, butter, buttermilk and 3/4 of the orange zest together (saving the rest for the filling). Add 2 cups of the flour along with the yeast and salt; stir until evenly moistened. Switch to the dough hook and add the remaining 1 3/4 cups flour and let the dough hook knead the mixture on low speed for 5 to 7 minutes. The dough should be soft and moist, but not overly sticky. Scrape the dough into a large, lightly oiled bowl (I usually scrape my dough briefly onto the counter, oil the mixing bowl, and scrape the dough back into it) and cover it with plastic wrap. Let dough rise at room temperature until doubled, which will take between 2 and 2 1/2 hours.

[Don’t have a stand mixer? Stir the mixture together with a wooden spoon, then continue stirring and beating it about in a large bowl for several minutes, until it comes together. Turn the dough out onto a floured counter and knead it for another 5 minutes. It will stick; don’t sweat it. Just scrape everything up and into the oiled bowl when it’s time to let it rise. Try to resist adding extra flour when it sticks; it will only toughen the dough. That would be sad.]

Prepare the filling: Melt the butter and set it aside. In a food processor, pulse the whole cranberries until they’re ground to a coarse rubble, but not fully pureed. You’ll need to scrape the machine down once or twice. Set them aside.

[Don’t have a food processor? Just hand chop them very well, as if to coarsely mince them.]

Assemble the buns: Butter a 9×13-inch baking dish, a heavier ceramic or glass dish is ideal here. Turn the risen dough out onto a floured work surface and roll it into a rectangle that is 18 inches wide (the side nearest to you) and 12 or so inches long. (It’s okay if it goes longer/thinner.) Brush the dough with the melted butter. Sprinkle it with the brown sugar. Scatter the ground cranberries over it, then the remaining orange zest.

Roll the dough into a tight, 18-inch long spiral. Using a sharp serrated knife, very, very gently saw the log into 1 1/2-inch sections; you should get 12. Arrange the buns evenly spread out in your baking dish. Cover with plastic wrap and refrigerate overnight, or up to 16 hours.

The next morning, bake the buns: Take your buns out of the fridge 30 minutes before you’d like to bake them, to allow them to warm up slightly. Heat your oven to 350 degrees F. Bake your buns until they’re puffed and golden (the internal temperature should read 190 degrees F), approximately 30 minutes.

Transfer pan to a cooling rack and let cool slightly. Make the icing by whisking the orange juice and powdered sugar together. Spread a little on each bun, or drizzle it over the whole pan. Serve immediately. ",
      source: "https://smittenkitchen.com/2013/11/cranberry-orange-breakfast-buns/",
      recipe_status: "Finalized",
      tags: Tag.where(name: ["Breakfast","Dessert"]))

#Recipe.find(1).tag_add(1)

Recipe.create(name: "Sloppy Joes",
  ingredients: "1 lb ground beef
    1 medium onion, chopped (1/2 cup)
    1/4 cup chopped celery

    1 cup ketchup
    1 tablespoon Worcestershire sauce
    1 teaspoon ground mustard
    1/8 teaspoon pepper
    6 hamburger buns",
  directions: "1.  In 10-inch skillet, cook beef, onion and celery over medium heat 8 to 10 minutes, stirring occasionally, until beef is brown; drain.

    2.  Stir in remaining ingredients except buns.  Heat to boiling; reduce heat.  Simmer uncovered 10 to 15 minutes, stirring occasionally, until vegetables are tender.

    3.  Fill buns with beef mixture.",
  source: "Betty Crocker Cookbook",
  recipe_status: "Finalized",
  tags: Tag.where(name: ["Dinner", "Leftovers Lunch", "Freezer Friendly"]))

Recipe.create(name: "Pancakes",
  ingredients: "Pancake mix:
    4 cups all-purpose flour
    3 tablespoons baking powder
    2 teaspoons baking soda
    1 teaspoon salt
    2 tablespoons plus 2 teaspoons sugar

    Pancake batter:
    1 egg
    1 cup milk
    1 tablespoon melted butter",
  directions: "1. Mix

    2. One batch of mix makes 8 medium sized pancakes.",
  source: "Alton Brown",
  recipe_status: "Finalized",
  tags: Tag.where(name: ["Breakfast"]))

Recipe.create(name: "Orecchiette, Ham, Parm, and Frozen Peas",
  ingredients: "1 pound of orecchiette
    handful frozen peas to taste
    salt and pepper
    shake or two of red pepper flakes
    olive oil
    1/2 small onion, chopped
    2 or 3 slices of Country Ham
    1/2 cup Parmesan, plus extra for serving
    1.5 tbsp butter",
  directions: "1.  Cook pasta according to package directions.  When it has one more minute of cooking, toss frozen peas into the water with the pasta. Reserve about 1/4 cup of pasta water then drain in a colander, drizzling a little olive oil into pasta to prevent sticking.

    2.  Return pot to the stove and over medium heat, add a few glugs of olive oil, onions, red pepper flakes, salt and pepper. Cook about a minute, stirring.

    3.  Add ham and cook until slightly crisp. Add pasta and peas to the pot and stir everything to combine. Add cheese and butter and a drizzle of reserved pasta water to make the cheese distribute evenly. Serve in bowls with more cheese and herbs.",
  source: "http://www.dinneralovestory.com/could-not-have-been-easier/",
  recipe_status: "Not tried",
  tags: Tag.where(name: ["Dinner", "Weeknight Meal", "Leftovers Lunch"]))

Recipe.create(name: "Meatballs",
  ingredients: "2 pounds ground sirloin
    1 cup Italian bread crumbs
    1 cup shredded Parmesean
    2 eggs
    1/2 cup chopped flat leaf parsley
    Few pinches fennel seeds
    2 teaspoons salt
    Freshly ground pepper",
  directions: "1.  In a large bowl, combine meat, bread crumbs, eggs, Parm, parsley, fennel, salt and pepper. Mash together with hands until thoroughly combined.

    2.  Roll into balls (the size of golf balls) and set aside on a plate.

    3.  In a large skillet, add a big glug of olive oil. Begin browning meatballs, in batches, over medium high heat, turning frequently. Remove when browned on all sides.

    4.  When all the meatballs are browned, add to sauce, and continue cooking over low heat for at least 30 minutes. Serve over pasta, pile high with cheese.",
  source: "http://www.dinneralovestory.com/great-grandma-turano%E2%80%99s-meatballs/",
  recipe_status: "In the works",
  tags: Tag.where(name: ["Freezer Friendly", "Dinner", "Leftovers Lunch"]))

Recipe.create(name: "Lemon cupcakes",
  ingredients: "½ cup unsalted butter
    1 cup sugar
    2 tbsp lemon zest

    2 eggs

    2 tsp vanilla
    1 ½ cup flour
    2 tsp baking powder
    ½ tsp salt

    ½ cup milk
    6 tbsp lemon juice",
  directions: "1.  Preheat oven to 350F.

    2.  Cream butter, sugar, vanilla, and lemon zest.

    3.  Add eggs, one at a time.

    4.  In a separate bowl mix flour, baking powder and salt.

    5.  Mix lemon juice in with the milk.

    6.  Alternate 1/3 of dry mixture with 1/3 of required milk, mixing well.

    7.  Bake in muffin tins for 18-20 minutes.",
  recipe_status: "In the works",
  tags: Tag.where(name: "Dessert"))

Recipe.create(name: "Honey Roasted Red Potatoes",
  ingredients: "1 pound red potatoes, quartered
    2 tablespoon diced onion
    2 tablespoon butter, melted
    1 tablespoon honey
    1 teaspoon dry mustard
    1 pinch salt
    1 pinch ground black pepper",
  directions: "1.  Preheat oven to 375F.

    2.  Lightly coat an 11x7 baking dish with nonstick cooking spray.

    3.  Place potatoes in a single layer in prepared dish, and top with onion.

    4.  In a small bowl combine melted butter, honey, mustard, salt, and pepper.  Drizzle over potatoes and onion.

    5.  Bake for 35 minutes or until tender.",
  source: "http://allrecipes.com/recipe/honey-roasted-red-potatoes/",
  recipe_status: "In the works",
  tags: Tag.where(name: ["Dinner", "Weeknight Meal"]))

Recipe.create(name: "Hibachi Shrimp",
  ingredients: "16 large shrimp (21/30), peeled, deveined, and cut down middle
    1.5 tbsp butter
    1 tbsp soy sauce
    1 tbsp lemon juice
    pepper",
  directions: "Heat wok or skillet over high heat.

    Add butter then shrimp.

    Add lemon juice and soy sauce.

    Stir fry until cooked, about 2 minutes.

    Turn over and continue to cook for an additional two minutes.",
  source: "http://crackedpepper2011.blogspot.com/2013/03/hibachi-shrimp-or-chicken-or-beef.html",
  recipe_status: "Finalized",
  tags: Tag.where(name: ["Dinner", "Weeknight Meal"]))

Recipe.create(name: "Hibachi Vegetables",
  ingredients: "1 tablespoon vegetable oil
    1/2 teaspoon sesame oil

    1/2 onion, sliced
    3 cups zucchini (or other vegetables like carrot, broccoli, etc.)

    1 tablespoon butter
    2 tablespoons soy sauce
    salt
    pepper",
  directions: "Heat the vegetable and sesame oil in a large skillet or wok on medium high heat.

    Add onion and other vegetables.

    Saute for 4-5 minutes or until crisp-tender.

    Add butter, soy sauce, salt, and pepper to taste.",
  source: "http://crackedpepper2011.blogspot.com/2013/03/hibachi-shrimp-or-chicken-or-beef.html",
  recipe_status: "Finalized")
