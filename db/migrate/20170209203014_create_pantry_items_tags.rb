class CreatePantryItemsTags < ActiveRecord::Migration[5.0]
  def change
    create_table :pantry_items_tags, id:false do |t|
      t.integer :pantry_item_id, null:false
      t.integer :tag_id, null:false
    end

    add_index :pantry_items_tags, [:pantry_item_id, :tag_id]
  end
end
