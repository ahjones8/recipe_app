class CreateRecipesTags < ActiveRecord::Migration[5.0]
  def change
    create_table :recipes_tags do |t|
      t.integer :recipe_id, null: false
      t.integer :tag_id, null: false
    end

    #add index to speed lookup
    add_index :recipes_tags, [:recipe_id, :tag_id], unique: true
  end
end
