class AddColumnsToRecipes < ActiveRecord::Migration[5.0]
  def change
    add_column :recipes, :notes, :text
    add_column :recipes, :source, :string
    add_column :recipes, :category, :string
  end
end
