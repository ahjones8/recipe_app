class AddFinishedToMealPlan < ActiveRecord::Migration[5.0]
  def change
    add_column :meal_plans, :finished, :boolean, null: false, default: false
  end
end
