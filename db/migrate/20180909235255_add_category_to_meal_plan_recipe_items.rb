class AddCategoryToMealPlanRecipeItems < ActiveRecord::Migration[5.0]
  def change
    add_reference :meal_plan_recipe_items, :category, index: true
    #add_foreign_key :meal_plan_recipe_items, :category
  end
end
