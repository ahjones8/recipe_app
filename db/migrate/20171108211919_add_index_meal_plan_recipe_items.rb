class AddIndexMealPlanRecipeItems < ActiveRecord::Migration[5.0]
  def change
    add_index :meal_plan_recipe_items, [:meal_plan_id, :recipe_id]
  end
end
