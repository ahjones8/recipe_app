class CreateMealPlanRecipeItems < ActiveRecord::Migration[5.0]
  def change
    create_table :meal_plan_recipe_items do |t|
      t.references :recipe, foreign_key: true
      t.belongs_to :meal_plan, foreign_key: true

      t.timestamps
    end
  end
end
