class ChangeCategoryColumnOnRecipe < ActiveRecord::Migration[5.0]
  def change
    rename_column :recipes, :category, :recipe_status
  end
end
