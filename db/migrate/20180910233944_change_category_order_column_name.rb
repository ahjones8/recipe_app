class ChangeCategoryOrderColumnName < ActiveRecord::Migration[5.0]
  def change
    rename_column :categories, :order, :order_num
  end
end
