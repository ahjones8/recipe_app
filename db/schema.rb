# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180910233944) do

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.integer  "order_num"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_categories_on_name", unique: true
    t.index ["order_num"], name: "index_categories_on_order_num", unique: true
  end

  create_table "meal_plan_recipe_items", force: :cascade do |t|
    t.integer  "recipe_id"
    t.integer  "meal_plan_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "category_id"
    t.index ["category_id"], name: "index_meal_plan_recipe_items_on_category_id"
    t.index ["meal_plan_id", "recipe_id"], name: "index_meal_plan_recipe_items_on_meal_plan_id_and_recipe_id"
    t.index ["meal_plan_id"], name: "index_meal_plan_recipe_items_on_meal_plan_id"
    t.index ["recipe_id"], name: "index_meal_plan_recipe_items_on_recipe_id"
  end

  create_table "meal_plans", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "finished",   default: false, null: false
  end

  create_table "pantry_items", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pantry_items_tags", id: false, force: :cascade do |t|
    t.integer "pantry_item_id", null: false
    t.integer "tag_id",         null: false
    t.index ["pantry_item_id", "tag_id"], name: "index_pantry_items_tags_on_pantry_item_id_and_tag_id"
  end

  create_table "recipes", force: :cascade do |t|
    t.string   "name"
    t.text     "ingredients"
    t.text     "directions"
    t.string   "source"
    t.text     "notes"
    t.string   "recipe_status"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "recipes_tags", force: :cascade do |t|
    t.integer "recipe_id", null: false
    t.integer "tag_id",    null: false
    t.index ["recipe_id", "tag_id"], name: "index_recipes_tags_on_recipe_id_and_tag_id", unique: true
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
